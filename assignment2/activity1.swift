//
//  activity1.swift
//  assignment2
//
//  Created by Gurlagan Bhullar on 2019-11-04.
//  Copyright © 2019 Gurlagan Bhullar. All rights reserved.
//

import Foundation
import Particle_SDK

class activity1: UIViewController {
    
    let USERNAME = "gurlaganbhullar@gmail.com"
    let PASSWORD = "12345"
    
    let DEVICE_ID = "420031000f47363333343437"
    var myPhoton : ParticleDevice?
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = UIImage(named: "rectangle")
        // Do any additional setup after loading the view.
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
                
            }
            
        } // end login
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToAllEvents(withPrefix: "rotateAngle", handler: {
            (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                //print("got event with data \(event?.data)")
                let choice = (event?.data)!
                print("ALL: \(choice)")
                let rotateAngle = (choice as NSString).floatValue
              //  UIView.animate(withDuration: 3.0, animations: {
                DispatchQueue.main.async {
                    self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat(rotateAngle))
                }
                
                
                
               // })
                
            }
        })
    }
    
    
    
}
