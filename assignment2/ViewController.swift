//
//  ViewController.swift
//  assignment2
//
//  Created by Gurlagan Bhullar on 2019-11-01.
//  Copyright © 2019 Gurlagan Bhullar. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    let USERNAME = "gurlaganbhullar@gmail.com"
    let PASSWORD = "12345"
     let randomNumber = Int.random(in: 1 ... 2)
    var count = 0
    
    @IBOutlet weak var showScoreLabel: UILabel!
    
    let DEVICE_ID = "420031000f47363333343437"
    var myPhoton : ParticleDevice?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        if(randomNumber == 1 ){
             imageView.image = UIImage(named: "triangle")
            
        }
        else{
            imageView.image = UIImage(named: "rectangle")
        }
       
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        } // end login
    }
    @IBAction func button(_ sender: Any) {
        if(randomNumber == 1 ){
            imageView.image = UIImage(named: "rectangle")
            
        }
        else{
            imageView.image = UIImage(named: "triangle")
        }
        showScore()
    
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                
                self.subscribeToParticleEvents()
                
            }
            
        } // end getDevice()
    }
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                DispatchQueue.main.async {
                if(self.imageView.image == UIImage(named: "triangle")){
                    
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    let coice = (event?.data)!
                    print(choice)
                    if (choice == "A") {
                        self.sendGreenColor()
                        print("Correct Answer")
                        self.count = self.count+1
                        
                        print(self.count)
                    }
                    else if (choice == "B") {
                        self.sendRedColor()
                        print("Wrong Answer")
                        
                    }
                    else if (choice == "C") {
                        self.sendRedColor()
                        print("Wrong Answer")
                    }
                    else if (choice == "D") {
                        self.sendRedColor()
                        print("Wrong Answer")
                    }
                    
                    
                    }}
                else if(self.imageView.image == UIImage(named: "rectangle")) {
                    if let _ = error {
                        print("could not subscribe to events")
                    } else {
                        print("got event with data \(event?.data)")
                        let choice = (event?.data)!
                        if (choice == "A") {
                            self.sendRedColor()
                            print("Wrong Answer")
                        }
                        else if (choice == "B") {
                            self.sendRedColor()
                            print("Wrong Answer")
                            
                        }
                        else if (choice == "C") {
                            self.sendGreenColor()
                            print("Correct Answer")
                            
                            self.count = self.count+1
                            print(self.count)
                        }
                        else if (choice == "D") {
                            self.sendRedColor()
                            print("Wrong Answer")
                        }
                        
                        
                    }
                }
                     self.showScoreLabel.text = " Score: " + String(self.count)
                    //self.showScore()
                }
               
               
                
        })
        
    }

    
    func sendGreenColor() {
        let parameters = ["green"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
 
        
    }
    
    func sendRedColor() {
    let parameters = ["red"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
   }
    func showScore(){
        print("show score on particle")
        
        let sendThis = ["\(count)"]
        var task = myPhoton!.callFunction("score", withArguments: sendThis) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to show score")
            }
            else {
                print("Error when telling Particle to show score")
            }
        }
    }


    }




