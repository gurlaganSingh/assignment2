#include <InternetButton.h>
#include "math.h"

InternetButton button = InternetButton();
void setup() {
  button.begin();

  Particle.function("score", displayScore);
  Particle.function("answer", answer);
}

int answer(String cmd) {
  if (cmd == "green") {
    button.allLedsOn(0,20,0);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "red") {
    button.allLedsOn(20,0,0);
    delay(2000);
    button.allLedsOff();
  }
  else {
    return -1;
  }
  return 1;
}

void loop() {
    
    int Xposition,Yposition;
    char Angle[15];
    int xValue = button.readX();
    int yValue = button.readY();
    
    int angleRotate = atan2(yValue - 0, xValue - 0);
    angleRotate = angleRotate * 180 / M_PI;


  if (button.buttonOn(1)) {
        sprintf(Angle, "%d", angleRotate);
         Particle.publish("rotateAngle",(Angle), 60, PUBLIC);
          Particle.publish("playerChoice","A", 60, PRIVATE);
   // button.playSong("C4,8,E4,8,G4,8,C5,8,G5,4");
    delay(500);
  }
  if (button.buttonOn(2)) {
        Xposition = button.readX();
        Yposition = button.readY();
        
    Particle.publish("playerChoice","B", 60, PRIVATE);
    delay(500);
  }
   if (button.buttonOn(3)) {
    Particle.publish("playerChoice","C", 60, PRIVATE);
    delay(500);
  }
   if (button.buttonOn(4)) {
    Particle.publish("playerChoice","D", 60, PRIVATE);
    delay(500);
  }

  if (button.buttonOn(1) && button.buttonOn(2)) {
    Particle.publish("nextQuestion","true", 60, PRIVATE);
    delay(500);
  }
}

int displayScore(String cmd) {
  button.allLedsOff();
  int score = cmd.toInt();

  if (score < 0 || score > 11) {
    return -1;
  }

  for (int i = 1; i <= score; i++) {
      button.ledOn(i, 255, 255, 0);
  }

  return 1;
}

